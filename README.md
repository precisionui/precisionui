# PrecisionUI
A Nintendo Switch UI that tries to stay close to the original UI designed by Nintendo
## Usage
Clone precisionui down and add the lib folder to your `SOURCES` and `INCLUDES`
## Changelog
View the changelog at [CHANGELOG.md](/CHANGELOG.md).
## Contributing
View the contribution guide at [CONTRIBUTING.md](/CONTRIBUTING.md)
## Credit
* Uses some code created by [Joel16](https://github.com/Joel16) that is licensed under GPLv3


## Horizon UI Details
Found at [info/README.md](/info/README.md)