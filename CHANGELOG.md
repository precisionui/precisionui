# Changelog
## [0.1.0-alpha.1](https://gitlab.com/precisionui/precisionui/tree/0.1.0-alpha.1) - 2018-06-15
### Added
* PrecisionUI base source code
* Sidebar (mimicks Switch system UI)
* Touch-screen handling (you can tap elements like a button, and switch spaces in the sidebar with touch!)
* Sidebar vs. Content Area focus (use the right button and left button to switch to content area and sidebar area focus respectively)
* Top/bottom dividers
* Theme support (automatically select theme based on the Switch's system UI theme)
* Added CHANGELOG.md

### Changed
### Deprecated
### Removed
### Fixed
### Security
