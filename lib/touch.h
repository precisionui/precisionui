#pragma once
#include "precisionui.h"

bool PUI_Touch_Touching(u32 touch_count, int x1, int y1, int x2, int y2);
int PUI_Touch_Sidebar_Touching(u32 touch_count);
int PUI_Touch_Element_Touching(u32 touch_count);
