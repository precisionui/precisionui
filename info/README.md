# PrecisionUI Switch UI information
## Pixel Measurements from Screenshots
*Screenshots 1280x720px @ 72dpi*
### Title Text
*Measurements for title retrieved from dark-datentime.jpg*  
**Top Left:** (72, 40)  
**Bottom Left:** (72, 61)  
### Top Divider
**Start:** (30, 87)  
**End:** (1249, 87)  
**Length:** 1219px
### Bottom Divider
**Start:** (30, 647)  
**End:** (1249, 647)  
**Length:** 1219px
### Sidebar
**Start (Top Left):** (0, 88)  
**End (Bottom Left):** (409, 646)  
**Content Area:** (469, 130) -> (1190, 632)   
**Item Start:** (100, 140)  
**Accent Bar:** (88, 129) (91, 180) w,h(91-88, 180-129) = (3, 51)  
**Next:** (101, 246)  
**Spacing:** 70px  
**Accent Bar Formula:** `(x - (100-88), y - (140-129))`
**Next Divider Formula:** `(80, (y - (140-129) + 25)) x2,y2(380, (y - (140-129) + 25))`  

TODO: complete